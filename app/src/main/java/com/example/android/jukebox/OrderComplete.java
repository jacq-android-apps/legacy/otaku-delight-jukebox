package com.example.android.jukebox;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import java.util.Random;

public class OrderComplete extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_complete);

        TextView songList = (TextView) findViewById(R.id.title_songList);
        Typeface typeface = Typeface.createFromAsset(OrderComplete.this.getAssets(), "fonts/njnaruto.ttf");
        songList.setTypeface(typeface);

        Random ran = new Random();
        int [] ranNum = new int [9];
        for(int i = 0; i < ranNum.length; i++){
            ranNum[i] = ran.nextInt();
        }

        TextView msg = (TextView) findViewById(R.id.msg);
        msg.setText("Order Confirmation Number: " + ranNum);
        msg.setTypeface(typeface);
    }
}
