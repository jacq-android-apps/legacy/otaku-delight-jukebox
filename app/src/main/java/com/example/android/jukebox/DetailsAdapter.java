package com.example.android.jukebox;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Jacquelyn Gboyor on 10/9/2017.
 */

public class DetailsAdapter extends ArrayAdapter<Details>{

    public DetailsAdapter(Activity context, ArrayList<Details> detail) {
        super(context, 0, detail);
    }
    int i = 0;
    int k = 0;
    SeekBar sb2;
    ImageButton pause;
    ImageButton play;
    Boolean isPlaying = true;
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }
        sb2 = (SeekBar) listItemView.findViewById(R.id.seekBar2);

        ImageButton mute = (ImageButton) listItemView.findViewById(R.id.volume_off);
        mute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = 0;
                sb2.setProgress(i);
            }
        });

        ImageButton vol_down = (ImageButton) listItemView.findViewById(R.id.volume_down);
        vol_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = i;
                sb2.setProgress(i);
                i = i - 15;
            }
        });

        ImageButton vol_up = (ImageButton) listItemView.findViewById(R.id.volume_up);
        vol_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = i;
                sb2.setProgress(i);
                i = i + 15;
            }
        });

        play = (ImageButton) listItemView.findViewById(R.id.play_song);
        //pause = (ImageButton) listItemView.findViewById(R.drawable.pause);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isPlaying){
                    play.setImageResource(R.drawable.pause);
                    isPlaying = false;
                }else {
                        play.setImageResource(R.drawable.play);
                        play.setTag(70);
                        isPlaying = true;
                    }

            }
        });

        Details currentGuide = getItem(position);

        ImageView pic = (ImageView) listItemView.findViewById(R.id.pic_placement);
        pic.setImageResource(currentGuide.getAlbum_pic());

        TextView length = (TextView) listItemView.findViewById(R.id.song_length);
        length.setText(currentGuide.getLength());

        TextView title = (TextView) listItemView.findViewById(R.id.song_title);
        title.setText(currentGuide.getSongTitle());

        TextView name = (TextView) listItemView.findViewById(R.id.artist_name);
        name.setText(currentGuide.getArtistName());

        return listItemView;
    }

}
