package com.example.android.jukebox;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class songs_spirited2 extends Fragment {


    public songs_spirited2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.list_view, container, false);

        ArrayList<Details> detail = new ArrayList<Details>();
        detail.add(new Details(R.drawable.spiritedaway_album, getString(R.string.spirited2Length),
                getString(R.string.spiritedS2), getString(R.string.SpiritedS2N)));

        DetailsAdapter itemsAdapter = new DetailsAdapter(getActivity(), detail);
        ListView view = (ListView) v.findViewById(R.id.list);
        view.setAdapter(itemsAdapter);

        return v;
    }

}
