package com.example.android.jukebox;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class Ponyo_Fragment extends Fragment {


    public Ponyo_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_ponyo_, container, false);

        Button sp1 = (Button) v.findViewById(R.id.ponyoS1);
        sp1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_ponyo()).addToBackStack(null).commit();

            }
        });//Spirited

        Button sp2 = (Button) v.findViewById(R.id.ponyoS2);
        sp2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_ponyos2()).addToBackStack(null).commit();
            }
        });//Spirited

        Button sp3 = (Button) v.findViewById(R.id.ponyoS3);
        sp3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_ponyo3()).addToBackStack(null).commit();
            }
        });//Spirited

        return v;
    }

}
