package com.example.android.jukebox;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

public class HomeFragment extends Fragment {

    public HomeFragment() {
        // Required empty public constructor

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_home, container, false);


        /*******Fonts for Titles*************/
        TextView myTextView = (TextView) v.findViewById(R.id.spirit1);
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/njnaruto.ttf");
        myTextView.setTypeface(typeface);

        myTextView = (TextView) v.findViewById(R.id.totoro1);
        Typeface typeface2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/njnaruto.ttf");
        myTextView.setTypeface(typeface2);

         myTextView = (TextView) v.findViewById(R.id.howl1);
        Typeface typeface3 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/njnaruto.ttf");
        myTextView.setTypeface(typeface3);

        myTextView = (TextView) v.findViewById(R.id.kiki1);
        Typeface typeface4 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/njnaruto.ttf");
        myTextView.setTypeface(typeface4);

        myTextView = (TextView) v.findViewById(R.id.ponyo1);
        Typeface typeface5 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/njnaruto.ttf");
        myTextView.setTypeface(typeface5);

        myTextView = (TextView) v.findViewById(R.id.mononoke1);
        Typeface typeface6 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/njnaruto.ttf");
        myTextView.setTypeface(typeface6);
        // Inflate the layout for this fragment

/*************Spirited Away****************/
        ImageButton sp1 = (ImageButton) v.findViewById(R.id.spirited);
        sp1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                //Fragment newFragment = new Spirited_Away();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new Spirited_Away()).addToBackStack(null).commit();
            }
        });//Spirited
/*************Totoro****************/
        ImageButton totoro1 = (ImageButton) v.findViewById(R.id.totoro);
        totoro1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new totoro()).addToBackStack(null).commit();
            }
        });//Spirited
/*************Howl****************/
        ImageButton howl1 = (ImageButton) v.findViewById(R.id.howl);
        howl1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new Howl()).addToBackStack(null).commit();
            }
        });//Spirited
/*************KIKI****************/
        ImageButton kiki1 = (ImageButton) v.findViewById(R.id.kiki);
        kiki1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new Kiki_Fragment()).addToBackStack(null).commit();
            }
        });//Spirited
/*************Ponyp****************/
        ImageButton ponyo1 = (ImageButton) v.findViewById(R.id.ponyo);
        ponyo1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new Ponyo_Fragment()).addToBackStack(null).commit();
            }
        });//Spirited
/*************Ponyp****************/
        ImageButton mononoke1 = (ImageButton) v.findViewById(R.id.mononoke);
        mononoke1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new Mononoke_Fragment()).addToBackStack(null).commit();
            }
        });//Spirited


        return v;
    }//on create

}//class
