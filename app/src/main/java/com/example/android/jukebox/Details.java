package com.example.android.jukebox;

/**
 * Created by Jacquelyn Gboyor on 10/9/2017.
 */

public class Details {

    private int album_pic;
    private String length;
    private String songTitle;
    private String artistName;

    public Details(int album_placement, String song_length, String song_title, String artist_name){
        album_pic = album_placement;
        length = song_length;
        songTitle = song_title;
        artistName = artist_name;
    }

    public int getAlbum_pic(){
        return album_pic;
    }
    public String getLength(){
        return length;
    }
    public String getSongTitle(){
        return songTitle;
    }
    public String getArtistName(){
        return artistName;
    }

}//class
