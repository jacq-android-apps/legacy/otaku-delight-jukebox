package com.example.android.jukebox;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class LibraryFragment extends Fragment {


    public LibraryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_library, container,false);

        /*************Princess Mononoke *********************************/
        Button sp1 = (Button) v.findViewById(R.id.mononokeS1);
        sp1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_mononoke()).addToBackStack(null).commit();
            }
        });//Spirited
        Button sp2 = (Button) v.findViewById(R.id.mononokeS2);
        sp2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_mononoke2()).addToBackStack(null).commit();
            }
        });//Spirited
        Button sp3 = (Button) v.findViewById(R.id.mononokeS3);
        sp3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_mononoke3()).addToBackStack(null).commit();
            }
        });//Spirited
//*************Spirited Away *********************************//
        Button sp4 = (Button) v.findViewById(R.id.spiritedS1);
        sp4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new Songs_Spirited()).addToBackStack(null).commit();
            }
        });//Spirited
        Button sp5 = (Button) v.findViewById(R.id.spiritedS2);
        sp5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_spirited2()).addToBackStack(null).commit();
            }
        });//Spirited
        Button sp6 = (Button) v.findViewById(R.id.spiritedS3);
        sp6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_spirited3()).addToBackStack(null).commit();
            }
        });//Spirited

//*************Totoro *********************************//
        Button sp7 = (Button) v.findViewById(R.id.totoroS1);
        sp7.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new song_totoro()).addToBackStack(null).commit();
            }
        });//Spirited
        Button sp8 = (Button) v.findViewById(R.id.totoroS2);
        sp8.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new song_totoro2()).addToBackStack(null).commit();
            }
        });//Spirited
        Button sp9 = (Button) v.findViewById(R.id.totoroS3);
        sp9.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new song_totoro3()).addToBackStack(null).commit();
            }
        });//Spirited

//*************Howl*********************************//
        Button sp10 = (Button) v.findViewById(R.id.howlS1);
        sp10.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_howl()).addToBackStack(null).commit();
            }
        });//Spirited
        Button sp11 = (Button) v.findViewById(R.id.howlS2);
        sp11.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_howl2()).addToBackStack(null).commit();
            }
        });//Spirited
        Button sp12 = (Button) v.findViewById(R.id.howlS3);
        sp12.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_howl3()).addToBackStack(null).commit();
            }
        });//Spirited

//*************Kiki *********************************//
        Button sp13 = (Button) v.findViewById(R.id.kikiS1);
        sp13.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_kiki()).addToBackStack(null).commit();

            }
        });//Spirited
        Button sp14 = (Button) v.findViewById(R.id.kikiS2);
        sp14.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_kiki2()).addToBackStack(null).commit();
            }
        });//Spirited
        Button sp15 = (Button) v.findViewById(R.id.kikiS3);
        sp15.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_kiki3()).addToBackStack(null).commit();
            }
        });//Spirited

//*************Ponyo *********************************//
        Button sp16 = (Button) v.findViewById(R.id.ponyoS1);
        sp16.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_ponyo()).addToBackStack(null).commit();
            }
        });//Spirited
        Button sp17 = (Button) v.findViewById(R.id.ponyoS2);
        sp17.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_ponyos2()).addToBackStack(null).commit();
            }
        });//Spirited
        Button sp18 = (Button) v.findViewById(R.id.ponyoS3);
        sp18.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new songs_ponyo3()).addToBackStack(null).commit();
            }
        });//Spirited
        return v;
    }

}
