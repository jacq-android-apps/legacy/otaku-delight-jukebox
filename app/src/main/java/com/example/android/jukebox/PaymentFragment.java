package com.example.android.jukebox;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import java.text.DecimalFormat;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentFragment extends Fragment {


    public PaymentFragment() {
        // Required empty public constructor
    }
    //Variables
    CheckBox box1, box2, box3, box4, box5, box6, box7, box8, box9, box10,
                    box11, box12, box13, box14, box15, box16, box17, box18;
    private static double cost = 0;
    double price1 = 0, price2 = 0, price3 = 0, price4 = 0, price5 = 0, price6 = 0,
            price7 = 0, price8 = 0, price9 = 0, price10 = 0, price11 = 0, price12 = 0,
            price13 = 0, price14 = 0, price15 = 0, price16 = 0, price17 = 0, price18 = 0;
    int quant1 = 0, quant2 = 0, quant3 = 0, quant4 = 0, quant5 = 0, quant6 = 0,
            quant7 = 0, quant8 = 0, quant9 = 0, quant10 = 0, quant11 = 0, quant12 = 0,
            quant13 = 0, quant14 = 0, quant15 = 0, quant16 = 0, quant17 = 0, quant18 = 0;
    DecimalFormat df2;
    TextView price_Total;
    TextView quantity;
    Button buy;
    Intent intent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_payment, container, false);

        df2 = new DecimalFormat(".##");
        buy = v.findViewById(R.id.buy);
        price_Total = v.findViewById(R.id.totalCost);
        quantity =  v.findViewById(R.id.quantity);
        intent = new Intent(getActivity(), PaymentConfirm.class);

        box1 = v.findViewById(R.id.S1); box2 = v.findViewById(R.id.S2);
        box3 = v.findViewById(R.id.S3); box4 = v.findViewById(R.id.S4);
        box5 = v.findViewById(R.id.S5); box6 = v.findViewById(R.id.S6);
        box7 = v.findViewById(R.id.S7); box8 = v.findViewById(R.id.S8);
        box9 = v.findViewById(R.id.S9); box10 = v.findViewById(R.id.S10);
        box11 = v.findViewById(R.id.S11); box12 = v.findViewById(R.id.S12);
        box13 = v.findViewById(R.id.S13); box14 = v.findViewById(R.id.S14);
        box15 = v.findViewById(R.id.S15); box16 = v.findViewById(R.id.S16);
        box17 = v.findViewById(R.id.S17); box18 = v.findViewById(R.id.S18);


        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            quantity.setText("Buy all");
            }
        });

        /********************************************************************/
        box1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price1 = 1.99;
                    quant1 = 1;
                }else{
                    price1 = 0;
                    quant1 = 0;
                }
                total();
                intent.putExtra("box1", box1.isChecked());
                //startActivity(intent);

            }
        });//End

        /********************************************************************/
        box2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price2 = 1.99;
                    quant2 = 1;
                }else{
                    price2 = 0;
                    quant2 = 0;
                }
                total();
                intent.putExtra("box2", box2.isChecked());
            }
        });

        /********************************************************************/
        box3.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price3 = 1.99;
                    quant3 = 1;
                }else{
                    price3 = 0;
                    quant3 = 0;
                }
                total();
                intent.putExtra("box3", box3.isChecked());
            }
        });

        /********************************************************************/
        box4.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price4 = 1.99;
                    quant4 = 1;
                }else{
                    price4 = 0;
                    quant4 = 0;
                }
                total();
                intent.putExtra("box4", box4.isChecked());
            }
        });

        /********************************************************************/
        box5.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price5 = 1.99;
                    quant5 = 1;
                }else{
                    price5 = 0;
                    quant5 = 0;
                }
                total();
                intent.putExtra("box5", box5.isChecked());
            }
        });

        /********************************************************************/
        box6.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price6 = 1.99;
                    quant6 = 1;
                }else{
                    price6 = 0;
                    quant6 = 0;
                }
                total();
                intent.putExtra("box6", box6.isChecked());
            }
        });

        /********************************************************************/
        box7.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price7 = 1.99;
                    quant7 = 1;
                }else{
                    price7 = 0;
                    quant7 = 0;
                }
                total();
                intent.putExtra("box7", box7.isChecked());
            }
        });

        /********************************************************************/
        box8.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price8 = 1.99;
                    quant8 = 1;
                }else{
                    price8 = 0;
                    quant8 = 0;
                }
                total();
                intent.putExtra("box8", box8.isChecked());
            }
        });

        /********************************************************************/
        box9.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price9 = 1.99;
                    quant9 = 1;
                }else{
                    price9 = 0;
                    quant9 = 0;
                }
                total();
                intent.putExtra("box9", box9.isChecked());
            }
        });

        /********************************************************************/
        box10.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price10 = 1.99;
                    quant10 = 1;
                }else{
                    price10 = 0;
                    quant10 = 0;
                }
                total();
                intent.putExtra("box10", box10.isChecked());
            }
        });

        /********************************************************************/
        box11.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price11 = 1.99;
                    quant11 = 1;
                }else{
                    price11 = 0;
                    quant11 = 0;
                }
                total();
                intent.putExtra("box11", box11.isChecked());
            }
        });

        /********************************************************************/
        box12.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price12 = 1.99;
                    quant12 = 1;
                }else{
                    price12 = 0;
                    quant12 = 0;
                }
                total();
                intent.putExtra("box12", box12.isChecked());
            }
        });

        /********************************************************************/
        box13.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price13 = 1.99;
                    quant13 = 1;
                }else{
                    price13 = 0;
                    quant13 = 0;
                }
                total();
                intent.putExtra("box13", box13.isChecked());
            }
        });

        /********************************************************************/
        box14.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price14 = 1.99;
                    quant14 = 1;
                }else{
                    price14 = 0;
                    quant14 = 0;
                }
                total();
                intent.putExtra("box14", box14.isChecked());
            }
        });

        /********************************************************************/
        box15.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price15 = 1.99;
                    quant15 = 1;
                }else{
                    price15 = 0;
                    quant15 = 0;
                }
                total();
                intent.putExtra("box15", box15.isChecked());
            }
        });

        /********************************************************************/
        box16.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price16 = 1.99;
                    quant16 = 1;
                }else{
                    price16 = 0;
                    quant16 = 0;
                }
                total();
                intent.putExtra("box16", box16.isChecked());
            }
        });

        /********************************************************************/
        box17.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price17 = 1.99;
                    quant17 = 1;
                }else{
                    price17 = 0;
                    quant17 = 0;
                }
                total();
                intent.putExtra("box17", box17.isChecked());
            }
        });

        /********************************************************************/
        box18.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()){
                    price18 = 1.99;
                    quant18 = 1;
                }else{
                    price18 = 0;
                    quant18 = 0;
                }
                total();
                intent.putExtra("box18", box18.isChecked());
            }
        });

        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PaymentConfirm.class);
                startActivity(intent);
                /*FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentLayout, new ConfirmationFragment()).addToBackStack(null).commit();*/
            }
        });

        return v;

    }//onCreate

    public void total(){
        cost = price1 + price2 + price3 + price4 + price5 + price6 +
                price7 + price8 + price9 + price10 + price11 + price12 +
                price13 + price14 + price15 + price16 + price17 + price18;
        price_Total.setText("$" + df2.format(cost));

        quantity.setText("" + (quant1 + quant2 + quant3 + quant4 + quant5 + quant6 + quant7 +
                quant8 + quant9 + quant10 + quant11 + quant12 + quant13 + quant14 + quant15 +
                quant16 + quant17 + quant18 + " Songs"));
    }

    public static double getCost(){
        return cost;
    }

}//class
