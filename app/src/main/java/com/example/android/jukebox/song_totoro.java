package com.example.android.jukebox;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class song_totoro extends Fragment {


    public song_totoro() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.list_view, container, false);

        ArrayList<Details> detail = new ArrayList<Details>();
        detail.add(new Details(R.drawable.totoro_album, getString(R.string.totoro1Length),
                getString(R.string.totoroS1), getString(R.string.totoroS1N)));

        DetailsAdapter itemsAdapter = new DetailsAdapter(getActivity(), detail);
        ListView view = (ListView) v.findViewById(R.id.list);
        view.setAdapter(itemsAdapter);

        return v;
    }

}
