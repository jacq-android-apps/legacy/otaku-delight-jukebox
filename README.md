# Otaku's Delight Jukebox
Android mock music player app I original created in 2017.

### 🗒️Overview
"The goal is to design and create the structure of a Music app which would allow a user to play audio files. 
There are many music player apps, and they make a wide variety of design choices. It will be your job to decide 
what kind of music app your structure would turn into and build out that structure using intents. Will you 
build an app to play music from the user’s library of music? Will you build an app to stream random songs 
from a database? Will you build a musical suggestion engine? Those choices are up to you!"

```
- Inspired by Studio Ghibli scores
- Actual music playing functionality not available
- App contains custom objects for storing information
- Contains custom adapter
- Use of Fragments
```

### 🏁 Getting Started

#### Prerequisites

To successfully build and run this app locally in Android Studio, you need to following:

| Config       | Details                   |
|--------------|---------------------------|
| Android SDK  | 8.0 (Oreo) API 26         |
| Gradle /     | v7.3.3                    |
| $agp_version | v7.2.0                    |
| Gradle JDK   | Jetbrains runtime v17.0.7 |

In file `./build.gradle` *buildscript.repositories* & *allprojects.repositories* contain:
```
google()
mavenCentral()
```

### 📷 Screenshots

|                             Welcome                              |                      Home                      |                       Library                        |
|:----------------------------------------------------------------:|:----------------------------------------------:|:----------------------------------------------------:|
| ![welcome screen](public/screens/welcome-page.webp){width=300px} | ![home](public/screens/home.webp){width=300px} | ![library](public/screens/library.webp){width=300px} |

|                 Howl's Moving Castle Library                 |               Kiki's Delivery Service Library                |                      Princess Mononoke Library                       |
|:------------------------------------------------------------:|:------------------------------------------------------------:|:--------------------------------------------------------------------:|
| ![Howl library](public/screens/album-howl.webp){width=300px} | ![Kiki library](public/screens/album-kiki.webp){width=300px} | ![Mononoke library](public/screens/album-mononoke.webp){width=300px} |

|                         Ponyo Library                          |                             Spirited Away Library                              |                    My Neighbor Totoro Library                    |
|:--------------------------------------------------------------:|:------------------------------------------------------------------------------:|:----------------------------------------------------------------:|
| ![Ponyo library](public/screens/album-ponyo.webp){width=300px} | ![Spirited away library](public/screens/album-spirited-away.webp){width=300px} | ![Totoro library](public/screens/album-totoro.webp){width=300px} |

|                          Song Playing                          |                         Song Paused                          |
|:--------------------------------------------------------------:|:------------------------------------------------------------:|
| ![song playing](public/screens/song-playing.webp){width=300px} | ![song paused](public/screens/song-paused.webp){width=300px} |

|                           Purchase Music                           |                              Purchase Music (Checked)                               |
|:------------------------------------------------------------------:|:-----------------------------------------------------------------------------------:|
| ![purchase music](public/screens/purchase-music.webp){width=300px} | ![purchase music checked](public/screens/purchase-music-selected.webp){width=300px} |

_Disclaimer: None of the images used to create this app belong to me._
